<?php

namespace Tests\Feature;

use App\DTO\SearchRequestInput;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class SearchBookTest extends TestCase
{
    const KEYWORD = 'اختصاصی';

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_search_book_api_response()
    {
        $response = $this->isLogin();

        $input = new SearchRequestInput();
        $input->setKeyWord(self::KEYWORD);
        $searchService = $this->app->make('App\Services\Search\Contract\SearchServiceInterface');
        $result = $searchService->search($input);
       
        $response->assertExactJson(['status' => true, 'data' => $result]);
    }

    private function isLogin()
    {
        $user = $this->createUser();
        $token = $this->loginAsUser($user);
        
        $response = $this->actingAs($user, 'api')
            ->post('/api/search/book', ['keyword' => self::KEYWORD], ['Authorization' => "Bearer $token"]);
        $response->assertStatus(200);

        return $response;
    }

    private function loginAsUser(User $user)
    {
        $token = JWTAuth::fromUser($user);

        return $token;
    }

    private function createUser()
    {
        User::truncate();
        $user = factory(User::class)->create();

        return $user;
    }
}
