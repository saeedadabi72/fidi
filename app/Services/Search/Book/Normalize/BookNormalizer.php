<?php

namespace App\Services\Search\Book\Normalize;

use App\DTO\Author;
use App\DTO\Publisher;
use App\DTO\SearchResponseInput;
use App\Services\Search\Contract\NormalizerInterface;

class BookNormalizer implements NormalizerInterface
{
    public function normalize(array $response)
    {
        if (!array_key_exists('books', $response)
            || !array_key_exists('hits', $response['books'])
            || !array_key_exists('hits', $response['books']['hits'])
        ) {
            return [];
        }

        $result = [];
        foreach ($response['books']['hits']['hits'] as $book) {
            $source = $book['_source'];
            $searchResponseInput = new SearchResponseInput();
            $searchResponseInput
                ->setImageName($source['image_name'])
                ->setId($source['id'])
                ->setTitle($source['title'])
                ->setContent($source['content'])
                ->setSlug($source['slug']);

            foreach ($source['publishers'] as $publisher) {
                $publisherInput = new Publisher();
                $publisherInput->setTitle($publisher);
                $searchResponseInput->setPublishers($publisherInput);
            }

            foreach ($source['authors'] as $author) {
                $authorInput = new Author();
                $authorInput->setName($author['name']);
                $searchResponseInput->setAuthors($authorInput);
            }

            $result[] = $searchResponseInput->toArray();
        }

        return $result;
    }
}
