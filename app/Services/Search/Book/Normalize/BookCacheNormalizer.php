<?php

namespace App\Services\Search\Book\Normalize;

use App\Constants\RedisConstants;
use App\Services\Search\Contract\NormalizerInterface;
use Illuminate\Cache\CacheManager;

class BookCacheNormalizer implements NormalizerInterface
{
    /**
     * @var CacheManager
     */
    private $cache;

    /**
     * @var BookNormalizer
     */
    private $normalizer;

    public function __construct(CacheManager $cache, BookNormalizer $normalizer)
    {
        $this->cache = $cache;
        $this->normalizer = $normalizer;
    }

    public function normalize(array $response)
    {
        return $this->cache->remember(
            RedisConstants::APPLICATION_PREFIX.RedisConstants::SEARCH_BOOK,
            RedisConstants::SEARCH_BOOK_TTL,
            function () use($response) {
                return $this->normalizer->normalize($response);
            }
        );
    }
}
