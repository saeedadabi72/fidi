<?php

namespace App\Services\Search;

use App\Client\AbstractSearchFidibo;
use App\DTO\SearchRequestInput;
use App\Services\Search\Contract\NormalizerInterface;
use App\Services\Search\Contract\SearchServiceInterface;

class SearchService implements SearchServiceInterface
{
    /**
     * @var AbstractSearchFidibo
     */
    private $search;

    /**
     * @var NormalizerInterface
     */
    private $normalizer;

    public function __construct(AbstractSearchFidibo $search, NormalizerInterface $normalizer)
    {
        $this->search = $search;
        $this->normalizer = $normalizer;
    }

    public function search(SearchRequestInput $input): array
    {
        $response = $this->search->search($input->toArray());

        return $this->normalizer->normalize($response);
    }
}
