<?php

namespace App\Services\Search\Contract;

use App\Client\AbstractSearchFidibo;
use App\DTO\SearchRequestInput;

interface SearchServiceInterface
{
    public function search(SearchRequestInput $input): array;
}
