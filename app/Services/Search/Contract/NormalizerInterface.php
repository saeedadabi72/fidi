<?php

namespace App\Services\Search\Contract;

interface NormalizerInterface
{
    public function normalize(array $response);
}
