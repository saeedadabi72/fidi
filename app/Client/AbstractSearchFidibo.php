<?php

namespace App\Client;

use Illuminate\Support\Facades\Http;

abstract class AbstractSearchFidibo
{
    abstract function search(array $data);

    protected function sendRequest(array $data, array $headers = [])
    {
        $response = Http::withHeaders($headers)
            ->post(env('SEARCH_FIDIBO_URL'), $data);

        return $response->json();
    }
}
