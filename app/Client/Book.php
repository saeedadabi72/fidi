<?php

namespace App\Client;

class Book extends AbstractSearchFidibo
{
    public function search(array $data)
    {
        $headers = [
            "Cache-Control" => "no-cache, must-revalidate, no-store, max-age=0, private",
        ];

        return $this->sendRequest($data, $headers);
    }
}
