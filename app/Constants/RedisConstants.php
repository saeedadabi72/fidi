<?php

namespace App\Constants;

class RedisConstants
{
    const APPLICATION_PREFIX = 'fidibo:';
    const SEARCH_BOOK = 'search_book';
    const SEARCH_BOOK_TTL = 10 * 60;
}
