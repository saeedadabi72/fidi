<?php

namespace App\DTO;

class SearchResponseInput
{
    /**
     * @var string
     */
    private $imageName;

    /**
     * @var Publisher[]
     */
    private $publishers;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var Author[]
     */
    private $authors;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): SearchResponseInput
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): SearchResponseInput
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): SearchResponseInput
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    public function getImageName(): string
    {
        return $this->imageName;
    }

    /**
     * @param string $imageName
     */
    public function setImageName(string $imageName): SearchResponseInput
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return Author
     */
    public function getAuthors(): Author
    {
        return $this->authors;
    }

    /**
     * @param Author $authors
     */
    public function setAuthors(Author $authors): SearchResponseInput
    {
        $this->authors[] = $authors;

        return $this;
    }

    /**
     * @return Publisher
     */
    public function getPublishers(): Publisher
    {
        return $this->publishers;
    }

    /**
     * @param Publisher $publishers
     */
    public function setPublishers(Publisher $publishers): SearchResponseInput
    {
        $this->publishers[] = $publishers;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): SearchResponseInput
    {
        $this->slug = $slug;

        return $this;
    }

    public function toArray()
    {
        $result = $authors = $publishers = [];
        foreach ($this->authors as $author) {
            $authors[] = [
                'name' => $author->getName(),
            ];
        }

        foreach ($this->publishers as $publisher) {
            $publishers[] = [
                'title' => $publisher->getTitle(),
            ];
        }

        return [
            'image_name' => $this->imageName,
            'publishers' => $publishers,
            'id' => $this->id,
            'title' => $this->title,
            'content' => $this->content,
            'slug' => $this->slug,
            'authors' => $authors,
        ];
    }
}
