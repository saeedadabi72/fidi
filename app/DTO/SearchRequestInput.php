<?php

namespace App\DTO;

class SearchRequestInput
{
    /**
     * @var string
     */
    private $keyWord;

    /**
     * @return string
     */
    public function getKeyWord(): string
    {
        return $this->keyWord;
    }

    /**
     * @param string $keyWord
     */
    public function setKeyWord(string $keyWord): void
    {
        $this->keyWord = $keyWord;
    }

    public function toArray()
    {
        return [
            'q' => $this->keyWord,
        ];
    }
}
