<?php

namespace App\Http\Controllers\Search;

use App\DTO\SearchRequestInput;
use App\Http\Controllers\Controller;
use App\Services\Search\Contract\SearchServiceInterface;
use Exception;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * @var SearchServiceInterface
     */
    private $searchService;

    public function __construct(SearchServiceInterface $searchService)
    {
        $this->middleware('auth:api');
        $this->searchService = $searchService;
    }

    public function search(Request $request)
    {
        $input = new SearchRequestInput();
        $input->setKeyWord($request->keyword);

        try {
            $result = $this->searchService->search($input);
        } catch(\Exception $e) {
            return $this->failed('internal server error');
        }

        return $this->success($result);
    }
}
