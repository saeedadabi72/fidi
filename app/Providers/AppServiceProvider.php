<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Client\AbstractSearchFidibo',
            'App\Client\Book'
        );

        $this->app->bind(
            'App\Services\Search\Contract\SearchServiceInterface',
            'App\Services\Search\SearchService'
        );

        $this->app->bind(
            'App\Services\Search\Contract\NormalizerInterface',
            'App\Services\Search\Book\Normalize\BookCacheNormalizer'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
